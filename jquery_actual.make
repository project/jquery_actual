core = 7.x
api = 2

; jQuery Actual plugin (jquery.actual) v1.0.19
libraries[jquery.actual][type] = libraries
libraries[jquery.actual][download][type] = git
libraries[jquery.actual][download][url] = git@github.com:dreamerslab/jquery.actual.git
libraries[jquery.actual][download][tag] = v1.0.19
libraries[jquery.actual][directory_name] = jquery.actual
libraries[jquery.actual][destination] = libraries
